<?php


class App{

    function __construct(){
        //echo "App¡¡ <br>";
        session_start();
    }
    public function login(){
        //echo "login¡¡ <br>";
        require 'viewLogin.php';
    }
    public function home(){
        echo "Lista de deseos";
        //require 'viewLogin.php';
        echo "<hr>";

        if (!isset($_SESSION['user'])) {
            header('Location:index.php?method=login');
            return;
        }
        $user = $_SESSION['user'];
        if (isset($_SESSION['deseos'])) {
            $deseos = $_SESSION['deseos'];
        }else{
            $deseos = [];
        }
        require 'viewHome.php';
    }
    public function auten(){
        //echo "Autentico!!";
        // tomar el usuario y abrir la sesion
        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])){
            $user = $_REQUEST['user'];
            $_SESSION['user'] = $user;
            header('Location:index.php?method=home');
        }else {
            header('Location:index.php?method=login');
        }
        return;
    }
    public function new(){

        if (isset($_REQUEST['deseo']) && !empty($_REQUEST['deseo'])) {
            $_SESSION['deseos'][] = $_REQUEST['deseo'];

            // $deseos = $_SESSION['deseos'];
            // $deseos[] = $_REQUEST['deseo'];
            // $_SESSION['deseos'] = $deseos;

        }
        header('Location:?method=home');
    }
    public function close(){
        echo "Close¡¡";
        session_destroy();
        unset($_SESSION);
        header('Location:index.php?method=login');
    }
    public function delete(){
        echo "Deleted  $_REQUEST[key]";
        $key = (integer) $_REQUEST['key'];
        unset($_SESSION['deseos'][$key]);
        header('Location:?method=home');
    }
    public function deleteAll(){
        echo "Se ha borrado todos!!";
        unset($_SESSION['deseos']);
        header('Location:?method=home');
    }
}
?>
