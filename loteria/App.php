<?php
class App{

    function __construct(){
        session_start();
        if (!isset($_SESSION['apuesta'])) {
            $_SESSION['apuesta'] = array();
        }
    }

    public function home(){
        require 'vistaInicio.php';
        foreach ($_SESSION['apuesta'] as $key => $value) {
            echo "$value" . " ";//mostramos los numero elegidos
        }// contamos los numeros que llevamos metidos en la apuesta
        $longitud = count($_SESSION['apuesta']);
        echo "<hr>";
        echo "$longitud Números seleccionados <br>";
        //dependiendo de los número apostados será simple o múltiple
        if ($longitud < 6) {
            echo " Necesitas al menos 6 números";
        }elseif ($longitud == 6) {
            echo " Números para apuesta simple";
        }else{
            echo " Apuesta múltiple";
        }
    }
    public function toggle(){
        require 'vistaInicio.php';
        echo "<hr>";
        $numero = $_REQUEST['numero'];
        if (empty($_SESSION['apuesta'])) {
            array_push($_SESSION['apuesta'],$numero);
            echo "Número añadido:  $numero" . " ";
            echo "<br>";
        }else{
            foreach ($_SESSION['apuesta'] as $key => $value) {
                //comprobar para borrar el número seleccionado
                if ($value == $numero) {
                    unset($_SESSION['apuesta'][$key]);
                    echo "Se ha borrado $value ";
                    header('Location:index.php');
                    exit();
                }
            }
            array_push($_SESSION['apuesta'],$numero);
            echo "<br>";
        }
        header('Location:index.php');
    }
}






