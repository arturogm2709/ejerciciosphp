<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <?php foreach ($array as $key => $imagen): ?>
        <!-- recorremos para mostrar -->
        <img width="100" src=<?php echo "uploads/$imagen" ?>>
        <li><?php echo $imagen ?></li>
        <a>
            <!-- mostramos el boton detalle para cada foto -->
            <form method="post" action="?method=detalle">
                <input type="hidden" name="detalle" value="<?php echo "$imagen" ?>">
                <input type="submit" value="Detalle">
            </form>
        </a>
        <hr>
        <a>
            <!-- recorremos para borrar en el caso de que queramos -->
            <form method="post" action="?method=borrar">
                <input type="hidden" name="file" value="<?php echo "uploads/$imagen" ?>">
                <input type="submit" value="Borrar" target="_self"><br><br>
            </form>
        </a>
    <?php endforeach ?>
    <h2><b><a href="index.php?method=home"> Volver a pantalla inicial </a></b></h2>


</body>
</html>
