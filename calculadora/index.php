<?php

require 'App.php';
$app = new App();

if (isset($_GET['method'])) {
    $method = $_GET['method'];

} else {
    $method = 'inicio';
}

if (!method_exists('App', $method)) {
    $method = 'inicio';
}


$app->$method();

?>
