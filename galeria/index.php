<?php

require 'App.php';
$app = new App();

if (isset($_GET['method'])) {
    $method = $_GET['method'];

} else {
    $method = 'home';
}

if (!method_exists('App', $method)) {
    $method = 'home';
}


$app->$method();

?>
