<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <h1>Calculadora</h1>
    <h3>Introduce tu operación: </h3>
    <form method="post" action="?method=resultado">
        <input  name="operador1">
        <select name="operacion">
            <option value="-">-</option>
            <option value="+">+</option>
            <option value="*">*</option>
            <option value="/">/</option>
        </select>
        <input  name="operador2">
        <input type="submit" value="=">
    </form>

    <h2>
        <?php echo $error ?>
    </h2>

    <?php if (isset($resultado)): ?>
        <h2>El resultado es: <?php echo $resultado ?></h2>
    <?php endif ?>

</body>
</html>
