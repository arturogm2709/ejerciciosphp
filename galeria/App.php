<?php
class App{
    public $error;

    function __construct()
    {
        session_start();

    }
    public function home(){
        require 'vistaInicio.php';
        $directorio = opendir("uploads/"); //ruta actual
        //sacamos los archivos del directorio
        $array = array();
        while ($archivo = readdir($directorio)) {
        //verificamos si es o no un directorio
            if (!is_dir($archivo)) {
                array_push($array,$archivo);
            //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
            } else {
        //echo $archivo . "<br />";
            }
        }
        require 'vistadeLista.php';
    }

    public function nueva()
    {
        require 'vistaGaleria.php';
    }
    public function add(){

        $subido = true;
        $uploadedfile_size = $_FILES['ficheroSubido']['size'];
        echo $_FILES['ficheroSubido']['name'];
        $msg = "";
        if ($_FILES['ficheroSubido']['size'] > 200000) {
            $msg = $msg."El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo<br>";
            $subido = false;
        }

        if (!($_FILES['ficheroSubido']['type'] =="image/jpeg" || $_FILES['ficheroSubido']['type'] =="image/gif")) {
            $msg = $msg." Tu archivo tiene que ser JPG o GIF.<br>";
            $subido = false;
        }
        $file_name = $_FILES['ficheroSubido']['name'];
        $add="uploads/$file_name";
        if ($subido) {
            if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {
                echo " Ha sido subido satisfactoriamente";
                echo "<br>";
                echo "<a href='?method=home'>Volver inicio </a>";
            } else {
                echo "Error al subir el archivo";
                echo "<br>";
                echo "<a href='?method=home'>Volver inicio </a>";
            }

        } else {
            echo "<br>";
            echo "<a href='?method=home'>Volver inicio </a>";
            echo $msg;
        }
    }


    public function borrar(){
        $imagen = $_POST['file'];
        unlink($imagen);
        echo "$imagen <br><br> Se ha borrado satisfactoriamnte";
        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "<a href='index.php?method=home'> Volver a pantalla de inicio </a>";
    }
    public function detalle(){
        $imagen=$_POST['detalle'];
        require "vistaDetalle.php";


    }


}
